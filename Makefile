dir = $(shell pwd)
volumes = -v ~/.ssh:/root/.ssh -v $(dir):/ansible
image = drentech/ansible
args = --env-file .env

docker:
	docker build -t drentech/ansible .
	docker push drentech/ansible

inventory:
	docker run --rm $(volumes) $(args) $(image) ansible-inventory -i hcloud.yml --graph

ping:
	docker run --rm $(volumes) $(args) $(image) ansible -i hcloud.yml all -m ping

all:
	docker run --rm $(volumes) $(args) $(image) ansible-playbook -i hcloud.yml site.yml;